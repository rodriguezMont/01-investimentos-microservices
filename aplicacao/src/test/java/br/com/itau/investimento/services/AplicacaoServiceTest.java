package br.com.itau.investimento.services;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Transacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;
import br.com.itau.investimento.repositories.TransacaoRepository;
import br.com.itau.investimento.viewobjects.Cliente;
import br.com.itau.investimento.viewobjects.Investimento;
import br.com.itau.investimento.viewobjects.Produto;

public class AplicacaoServiceTest {
//	AplicacaoService aplicacaoService;
//
//	AplicacaoRepository aplicacaoRepository;
//
//	TransacaoRepository transacaoRepository;
//	
//	ClienteService clienteService;
//	
//	ProdutoService produtoService;
//
//	Investimento investimento;
//	
//	@Before
//	public void preparar() {
//		criarMocks();
//		criarModels();
//	}
//
//	private void criarMocks() {
//		aplicacaoService = new AplicacaoService();
//		
//		aplicacaoRepository = Mockito.mock(AplicacaoRepository.class);
//		transacaoRepository = Mockito.mock(TransacaoRepository.class);
//		clienteService = Mockito.mock(ClienteService.class);
//		produtoService = Mockito.mock(ProdutoService.class);
//		
//		aplicacaoService.aplicacaoRepository = aplicacaoRepository;
//		aplicacaoService.transacaoRepository = transacaoRepository;
//		aplicacaoService.clienteService = clienteService;
//		aplicacaoService.produtoService = produtoService;
//	}
//
//	private void criarModels() {
//		investimento = new Investimento();
//		Transacao transacao = new Transacao();
//		Cliente cliente = new Cliente();
//		Produto produto = new Produto();
//		
//		cliente.setCpf("123.123.123-12");
//		investimento.setCliente(cliente);
//		
//		transacao.setValor(1000);
//		investimento.setTransacao(transacao);
//		
//		produto.setId(1);
//		produto.setRendimento(0.005);
//		investimento.setProduto(produto);
//	}
//
//	@Test
//	public void testarCriarAplicacao() {
//		definirRespostaBuscarCliente();
//		definirRespostaBuscarProduto();
//		definirRespostaCriarTransacao();
//		definirRespostaCriarAplicacao();
//		
//		Investimento retorno = aplicacaoService.criar(investimento).get();
//
//		assertNotNull(retorno.getAplicacao().getId());
//		assertNotNull(retorno.getTransacao().getId());
//	}
//	
//	private void definirRespostaBuscarCliente() {
//		Cliente cliente = new Cliente();
//		cliente.setCpf("123.123.123-12");
//		
//		when(clienteService.buscar(any(String.class)))
//			.thenReturn(Optional.of(cliente));
//	}
//	
//	private void definirRespostaBuscarProduto() {
//		Produto produto = new Produto();
//		produto.setId(1);
//		
//		when(produtoService.buscar(1))
//			.thenReturn(Optional.of(produto));
//		
//	}
//	
//	private void definirRespostaCriarTransacao() {
//		Transacao transacao = new Transacao();
//		transacao.setId(UUID.randomUUID());
//		transacao.setValor(1000);
//		
//		when(transacaoRepository.save(any(Transacao.class)))
//				.thenReturn(transacao);
//	}
//	
//	private void definirRespostaCriarAplicacao() {
//		Aplicacao aplicacao = new Aplicacao();
//		aplicacao.setId(1);
//		
//		when(aplicacaoRepository.save(any(Aplicacao.class)))
//				.thenReturn(aplicacao);
//	}
}
